#!/bin/bash
#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <j.jutteau@gmail.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return -- Jerome Jutteau
# ----------------------------------------------------------------------------
#
# This permits the user to recover from unwanted rm commands by putting files
# in a trashbin. If the trashbin size reaches it's maximum capacity, the script
# will remove the oldest files in order to put the newest files in the trash.
# If the file to remove is bigger than the trash, a deleting confirmation is
# asked.
# You can alias your rm command to this script but you must be aware the script
# don't understand any official rm options.
# For directories removal you don't need to specifie -r.
# You can specifie multiple targets to remove.

# Trashbin path.
trash_path=$HOME/.trash
# Trashbin limit (in Bytes).
trash_limit=200000000 # 200 MB of trash.
#trash_limit=1000000000 # 1 GB of trash.
# Real rm path.
rm=/bin/rm

# Function to free trash.
# It may definitly remove oldest files in trash.
function free_trash ()
{
	size=$1
	space_left=$(($trash_limit - $(du -bs "$trash_path" | cut -f 1)))
	# Free only if we do not have any space left.
	if (($size > $space_left)); then
		find "$trash_path" -maxdepth 1 -mindepth 1 | while read f; do
			$rm -rf "$f"
			space_left=$(($trash_limit - $(du -bs "$trash_path" | cut -f 1)))
			if (($size <= $space_left)); then
				break
			fi
		done
	fi
	return 0
}

# Propose to definitly remove a file.
function propose_remove ()
{
	file=$1
	echo "remove $file ? (y/N)"
	read response
	if [[ "$response" == "y" ]]; then
		$rm -rf "$file"
	fi
}

# Create the trashbin folder if it does not exist.
if ! test -d $trash_path; then
	mkdir -p $trash_path
	if ! test -d $trash_path; then
		echo "Failed to create $trash_path"
		exit 1
	fi
fi

# Check the trashbin size is not already bigger than configured.
trash_size=$(du -bs "$trash_path" | cut -f 1)
if (($trash_size > $trash_limit)); then
	free_trash $(($trash_size - $trash_limit))
fi

# Iterate all targets to remove.
for target in "$@"; do
	# Ensure target exists.
	if ! test -e "$target"; then
		echo "$target does not exist."
		continue
	fi
	target_size=$(du -bs "$target" | cut -f 1)
	target_basename=$(basename "$target")
	target_trash_path="$trash_path/$(date +%s%N)-${RANDOM}_${target_basename}"
	if (($target_size > $trash_limit)); then
		echo "$target too big to fit in trashbin."
		propose_remove "$target"
		continue
	fi
	# Ensure we have the space in trashbin.
	free_trash $target_size
	# Try to make a hard link (certainly won't work with directories).
	ln "$target" "$target_trash_path" &> /dev/null
	# Hard link success, we can remove original file location.
	if ! (($?)); then
		$rm -rf "$target"
	# Hard link failed, use move instead.
	else
		mv "$target" "$target_trash_path"
		# Move failed.
		if (($?)); then
			echo "Cannot move $target to $trash_path"
			propose_remove "$target"
			continue
		fi
	fi
done