A small script you should always alias to rm command.

Permits to move your deleted files to a .trash folder.

Also permits to limit total .trash folder size by deleting oldest files inside it.

Saved people from nervous breakdown since a while